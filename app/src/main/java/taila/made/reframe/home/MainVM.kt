package taila.made.reframe.home

import android.arch.lifecycle.*
import taila.made.reframe.thoughts.ThoughtRecord
import taila.made.reframe.utils.dev.Mocker

class MainVM : ViewModel(), LifecycleObserver {

    val thoughtRecord = MutableLiveData<ThoughtRecord>()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initThoughtRecord() {
        thoughtRecord.value = Mocker.createDefaultThoughtRecord()
    }

}