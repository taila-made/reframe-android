package taila.made.reframe.home

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import androidx.navigation.Navigation.findNavController
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import taila.made.reframe.R
import taila.made.reframe.databinding.LayoutFragmentHolderBinding


class MainActivity : AppCompatActivity() {

    lateinit var vm: MainVM
    lateinit var binding: LayoutFragmentHolderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        binding = DataBindingUtil.setContentView(this, R.layout.layout_fragment_holder)
        vm = ViewModelProviders.of(this).get(MainVM::class.java)
        lifecycle.addObserver(vm)
    }

    override fun onSupportNavigateUp() = findNavController(this, R.id.navHostFragment).navigateUp()

}