package taila.made.reframe.home

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import taila.made.reframe.R
import taila.made.reframe.databinding.ItemThoughtRecordBinding
import taila.made.reframe.databinding.ItemThoughtRecordsTimeBinding

class HomeThoughtRecordsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_THOUGHT_RECORD = 0
        const val TYPE_TIME = 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0 || position == 4 || position == 6) TYPE_TIME else TYPE_THOUGHT_RECORD
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_TIME) {
            val binding = DataBindingUtil.inflate<ItemThoughtRecordsTimeBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_thought_records_time,
                    parent,
                    false
            )
            TimeVH(binding)
        } else {
            val binding = DataBindingUtil.inflate<ItemThoughtRecordBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_thought_record,
                    parent,
                    false
            )
            ThoughtRecordVH(binding)
        }
    }

    override fun getItemCount(): Int = 13

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 4) {
            holder as TimeVH
            holder.binding.tvTime.text = "Yesterday"
        } else if (position == 6) {
            holder as TimeVH
            holder.binding.tvTime.text = "01/31/2018"
        }
    }

    inner class ThoughtRecordVH(val binding: ItemThoughtRecordBinding) : RecyclerView.ViewHolder(binding.root) {}
    inner class TimeVH(val binding: ItemThoughtRecordsTimeBinding) : RecyclerView.ViewHolder(binding.root) {}

}