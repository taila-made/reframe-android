package taila.made.reframe.home

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.widget.LinearLayoutManager
import android.transition.Fade
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import taila.made.reframe.App
import taila.made.reframe.R
import taila.made.reframe.databinding.FragmentHomeBinding
import taila.made.reframe.ui.AnimationUtil
import taila.made.reframe.ui.UIUtil
import taila.made.reframe.ui.shrinkTextAnimation
import taila.made.reframe.utils.FirebaseUtil
import javax.inject.Inject

class HomeFragment : Fragment() {

    @Inject
    lateinit var firebaseUtil: FirebaseUtil

    lateinit var binding: FragmentHomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        App.component.inject(this)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        UIUtil.addStatusBarMargin(binding.tvTitle)

        binding.btnNewThought.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_createThoughtRecordFragment)
        }
        binding.btnMenu.setOnClickListener {
            binding.rootV.openDrawer(GravityCompat.END)
        }
        binding.btnLogout.setOnClickListener {
            firebaseUtil.logout(activity!!)
        }

        (binding.firstCard.root as ConstraintLayout)
                .background.setTint(ContextCompat.getColor(context!!, R.color.aqua_blue))
        (binding.secondsCard.root as ConstraintLayout)
                .background.setTint(ContextCompat.getColor(context!!, R.color.lilac))
        (binding.thirdCard.root as ConstraintLayout)
                .background.setTint(ContextCompat.getColor(context!!, R.color.green))

        binding.fragmentHomeRv.adapter = HomeThoughtRecordsAdapter()
        binding.fragmentHomeRv.layoutManager = LinearLayoutManager(activity)

        return binding.root
    }

}