package taila.made.reframe.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import taila.made.reframe.networking.RetrofitUtil
import taila.made.reframe.ui.AnimationUtil
import taila.made.reframe.ui.UIUtil
import taila.made.reframe.utils.FirebaseUtil
import javax.inject.Singleton

@Module
class UtilModule {

    @Provides
    @Singleton
    internal fun retrofitUtil(): RetrofitUtil {
        return RetrofitUtil
    }

    @Provides
    @Singleton
    internal fun firebaseUtil(): FirebaseUtil {
        return FirebaseUtil()
    }

    @Provides
    internal fun firestore(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    @Provides
    internal fun firebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

}