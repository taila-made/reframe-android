package taila.made.reframe.di

import android.content.Context
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import taila.made.reframe.App
import taila.made.reframe.auth.AuthActivity
import taila.made.reframe.auth.AuthVM
import taila.made.reframe.home.HomeFragment
import taila.made.reframe.thoughts.CreateThoughtRecordFragment
import taila.made.reframe.ui.AnimationUtil
import taila.made.reframe.ui.UIUtil
import taila.made.reframe.utils.FirebaseUtil
import taila.made.reframe.utils.dev.FirebaseHelper
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, AndroidSupportInjectionModule::class, UtilModule::class])
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()

    fun app(): App
    fun context(): Context

    fun inject(activity: AuthActivity)
    fun inject(fragment: HomeFragment)
    fun inject(firebaseHelper: FirebaseHelper)
    fun inject(frag: CreateThoughtRecordFragment)
    fun inject(uiUtil: UIUtil)
    fun inject(firebaseUtil: FirebaseUtil)
    fun inject(animationUtil: AnimationUtil)
    fun inject(firestore: FirebaseFirestore)
    fun inject(vm: AuthVM)
}