package taila.made.reframe.di

import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import taila.made.reframe.App
import taila.made.reframe.LaunchActivity
import taila.made.reframe.auth.AuthActivity
import taila.made.reframe.auth.SignupActivity
import taila.made.reframe.home.HomeFragment
import taila.made.reframe.thoughts.CreateThoughtRecordFragment

@Module
abstract class AppModule {

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun auth(): AuthActivity

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun signup(): SignupActivity

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun createTR(): CreateThoughtRecordFragment

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun homeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun launch(): LaunchActivity
    
    @Binds
    abstract fun bindApp(app: App): Context
}