package taila.made.reframe

import android.util.Log
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import taila.made.reframe.di.AppComponent
import taila.made.reframe.di.DaggerAppComponent

class App : DaggerApplication() {

    companion object {
        lateinit var component: AppComponent
        const val TAG = "RFR:App"
    }

    override fun applicationInjector(): AndroidInjector<out App> {
        component = DaggerAppComponent.builder().create(this) as AppComponent
        component.inject(this)
        return component
    }

    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "onCreate()")
    }

    override fun onTerminate() {
        super.onTerminate()
        Log.i(TAG, "onTerminate()")
    }

}