package taila.made.reframe.thoughts

class ThoughtPart() {
    var title: String = ""
    var question: String = ""
    var subquestion: String = ""
    var answer: String? = null
    var value: Int? = null

    constructor(title: String, question: String, subquestion: String, answer: String? = null, value: Int? = null) : this() {
        this.title = title
        this.question = question
        this.subquestion = subquestion
        this.answer = answer
        this.value = value
    }
}