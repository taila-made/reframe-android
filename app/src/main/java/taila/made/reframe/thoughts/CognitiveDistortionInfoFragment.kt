package taila.made.reframe.thoughts

import android.animation.ValueAnimator
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.transition.Fade
import android.transition.TransitionInflater
import android.transition.TransitionSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.transition.doOnStart
import androidx.core.view.doOnPreDraw
import taila.made.reframe.R
import taila.made.reframe.databinding.FragmentCognitiveDistortionBinding
import taila.made.reframe.ui.AnimationUtil

class CognitiveDistortionInfoFragment : Fragment() {

    companion object {
        val KEY_TOP_BAR_HEIGHT = "top_bar_height"

        fun newInstance(topBarHeight: Float): CognitiveDistortionInfoFragment {
            val args = Bundle()
            args.putFloat(KEY_TOP_BAR_HEIGHT, topBarHeight)
            val fragment = CognitiveDistortionInfoFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var binding: FragmentCognitiveDistortionBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cognitive_distortion, container, false)

        setupTransitions()

        return binding.root
    }

    fun setupTransitions() {
        val sharedElementCardViewTransition = TransitionInflater.from(context)
                .inflateTransition(R.transition.shared_element_transition_chip_to_card_view)

        sharedElementCardViewTransition.doOnStart {
            val translateYAnimator = ValueAnimator.ofFloat(binding.fragmentCognitiveDistortionCardView.translationY, 0f)
            translateYAnimator.duration = AnimationUtil.DURATION_MOBILE_ENTERING
            translateYAnimator.addUpdateListener { valueAnimator ->
                binding.fragmentCognitiveDistortionCardView.translationY = valueAnimator.animatedValue as Float
            }
            translateYAnimator.start()
        }
        sharedElementEnterTransition = sharedElementCardViewTransition
        sharedElementReturnTransition = sharedElementCardViewTransition
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postponeEnterTransition()
        binding.fragmentCognitiveDistortionCardView.doOnPreDraw {
            if (arguments != null) {
                it.translationY = arguments!!.getFloat(KEY_TOP_BAR_HEIGHT, 0f)
            }
            startPostponedEnterTransition()
        }
    }


}