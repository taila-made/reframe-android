package taila.made.reframe.thoughts

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.design.chip.Chip
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import com.jakewharton.rxbinding2.widget.RxTextView
import taila.made.reframe.R
import taila.made.reframe.databinding.LayoutThoughtPartBinding
import taila.made.reframe.databinding.LayoutThoughtPartCognitiveDistortionsBinding
import taila.made.reframe.databinding.LayoutThoughtPartFeelingsBinding
import taila.made.reframe.ui.popAnimation
import taila.made.reframe.ui.transitions.ChipToCardViewTransition
import java.util.concurrent.TimeUnit


class ThoughtPartPagerAdapter(val fragment: CreateThoughtRecordFragment, val thoughtRecord: ThoughtRecord) : PagerAdapter() {
    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(container.context)
        val thoughtPart = thoughtRecord.thoughtParts[position]

        return when {
            thoughtPart.title.contains("Feelings and Emotions") -> {
                val binding = DataBindingUtil.inflate<LayoutThoughtPartFeelingsBinding>(inflater, R.layout.layout_thought_part_feelings, container, false)
                binding.thoughtPart = thoughtPart
                container.addView(binding.root)
                binding.chipGroupFeelings.children.forEach { view ->
                    val chip = view as Chip
                    chip.setOnClickListener {
                        if (chip.isSelected) {
                            unselectChip(chip)
                        } else {
                            selectChip(chip)
                        }
                    }
                }
                binding.root
            }
            thoughtPart.title.contains("Cognitive Distortions") -> {
                fragment.cognitiveDistortionPositionInViewPager = position
                val binding = DataBindingUtil.inflate<LayoutThoughtPartCognitiveDistortionsBinding>(inflater, R.layout.layout_thought_part_cognitive_distortions, container, false)
                binding.thoughtPart = thoughtPart
                container.addView(binding.root)
                for (i in 0 until binding.thoughtPartRoot.childCount) {
                    if (binding.thoughtPartRoot.getChildAt(i) is Chip) {
                        val chip = binding.thoughtPartRoot.getChildAt(i) as Chip
                        chip.setOnClickListener {
                            if (chip.isSelected) {
                                unselectChip(chip)
                            } else {
                                selectChip(chip)
                            }
                        }
                        chip.setOnLongClickListener {
                            startChipInfoActivity(chip)
                            true
                        }
                    }
                }
                binding.root
            }
            else -> {
                val binding = DataBindingUtil.inflate<LayoutThoughtPartBinding>(inflater, R.layout.layout_thought_part, container, false)
                binding.thoughtPart = thoughtPart
                val obs = RxTextView
                        .textChanges(binding.etAnswer)
                        .filter { charSequence -> charSequence.length > 4 }
                        .map { charSequence -> charSequence.toString() }

                obs.subscribe { string ->
                    thoughtPart.answer = string
                    updateThoughtPart(thoughtPart)
                }
                if (position + 1 == thoughtRecord.thoughtParts.size) {
                    binding.btnDone.visibility = View.VISIBLE
                }
                binding.etAnswer.visibility = View.VISIBLE
                container.addView(binding.root)
                binding.root
            }
        }
    }

    fun startChipInfoActivity(chip: Chip) {
        val activity = chip.context as Activity
        chip.transitionName = activity.getString(R.string.transition_name_chip_to_card)
        val chipExtras = ChipToCardViewTransition.makeExtrasBundle(chip)
        val intent = Intent(activity, CognitiveDistortionInfoActivity::class.java)
        intent.putExtra(activity.getString(R.string.transition_name_chip_to_card), chipExtras)
        val bundle = ActivityOptions.makeSceneTransitionAnimation(
                activity,
                chip,
                activity.getString(R.string.transition_name_chip_to_card)
        ).toBundle()
        activity.startActivity(intent, bundle)
        activity.overridePendingTransition(0, 0)
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        (container as ViewPager).removeView(obj as View)
    }

    fun selectChip(chip: Chip) {
        chip.popAnimation(
                toBgColor = ContextCompat.getColor(chip.context, R.color.pink)
        ).start()
        chip.setTextAppearanceResource(R.style.Chip_TextAppearance_White)
        chip.setChipIconResource(R.drawable.ic_info_outline_white_24dp)
        chip.isSelected = true
    }

    fun unselectChip(chip: Chip) {
        chip.popAnimation(
                toBgColor = ContextCompat.getColor(chip.context, android.R.color.white)
        ).start()
        chip.setTextAppearanceResource(R.style.Chip_TextAppearance)
        chip.setChipIconResource(R.drawable.ic_info_outline_text_color_purple_24dp)
        chip.isSelected = false
    }

    fun updateThoughtPart(thoughtPart: ThoughtPart) {
        fragment.onThoughtPartUpdated(thoughtPart)
    }

    override fun getCount(): Int {
        return thoughtRecord.thoughtParts.size
    }

}