package taila.made.reframe.thoughts

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.support.v7.app.AppCompatActivity
import android.transition.ArcMotion
import androidx.core.transition.doOnEnd
import androidx.core.view.doOnPreDraw
import taila.made.reframe.R
import taila.made.reframe.databinding.ActivityCognitiveDistortionInfoBinding
import taila.made.reframe.ui.AnimationUtil
import taila.made.reframe.ui.transitions.ChipToCardViewTransition

class CognitiveDistortionInfoActivity : AppCompatActivity() {

    companion object {
        val TAG = "TT"
    }

    lateinit var binding: ActivityCognitiveDistortionInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cognitive_distortion_info)

        val transitionChipToCardViewBundle = intent.extras.getBundle(getString(R.string.transition_name_chip_to_card))

        window.sharedElementEnterTransition = ChipToCardViewTransition(transitionChipToCardViewBundle).apply {
            addTarget(binding.fragmentCognitiveDistortionCardView)
            excludeTarget(binding.fragmentCognitiveDistortionCardViewContentWrapper, true)
            pathMotion = ArcMotion()
            duration = AnimationUtil.DURATION_MOBILE_ENTERING
            interpolator = FastOutSlowInInterpolator()
            doOnEnd {
                endValues = startValues
                startValues = null
            }
        }
        postponeEnterTransition()
        binding.fragmentCognitiveDistortionCardView.doOnPreDraw {
            startPostponedEnterTransition()
        }
    }

    override fun onBackPressed() {
        finishAfterTransition()
    }

}