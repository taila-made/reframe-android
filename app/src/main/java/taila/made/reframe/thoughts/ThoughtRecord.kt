package taila.made.reframe.thoughts

class ThoughtRecord constructor() {

    var thoughtParts: MutableList<ThoughtPart> = mutableListOf()

    constructor(thoughtParts: MutableList<ThoughtPart>) : this() {
        this.thoughtParts = thoughtParts
    }

    override fun toString(): String {
        return "ThoughtRecord(thoughtParts=$thoughtParts)"
    }
}