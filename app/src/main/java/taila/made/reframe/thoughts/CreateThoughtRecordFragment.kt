package taila.made.reframe.thoughts

import android.animation.AnimatorSet
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.transition.Fade
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.TextView
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import taila.made.reframe.App
import taila.made.reframe.R
import taila.made.reframe.databinding.FragmentCreateThoughtRecordBinding
import taila.made.reframe.home.MainActivity
import taila.made.reframe.ui.AnimationUtil
import taila.made.reframe.ui.FadeParralaxTransformer

class CreateThoughtRecordFragment : Fragment() {

    lateinit var binding: FragmentCreateThoughtRecordBinding
    var cognitiveDistortionPositionInViewPager = -1

    companion object {
        const val TAG = "TT:CreateTRAct"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.component.inject(this)
        super.onCreate(savedInstanceState)
//        FirebaseHelper().storeDefaultThoughtRecord("1")
    }

    // TODO: Only init viewpager once we have a thought record (maybe needs a loading placeholder)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_thought_record, container, false)
        setupViews()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val mEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.transition_create_thought_record_enter)
        enterTransition = Fade().apply {
            duration = AnimationUtil.DURATION_MOBILE_ENTERING
        }
        returnTransition = mEnterTransition.apply {
            duration = AnimationUtil.DURATION_MOBILE_EXITING
        }
        postponeEnterTransition()

        binding.root.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                binding.root.viewTreeObserver.removeOnPreDrawListener(this)

                startPostponedEnterTransition()
                return true
            }
        })
    }

    var played = false

    fun showTooltip() {
        val animatorSet = AnimatorSet()
        val view = binding.root.findViewById<TextView>(R.id.tooltip)
        val fadeInAnimator = AnimationUtil.fade(view, 0f, 1f).apply {
            duration = 1000
        }
        val fadeOutAnimator = AnimationUtil.fade(view, 1f, 0f).apply {
            startDelay = 5000
            duration = 1000
        }
        animatorSet.playSequentially(fadeInAnimator, fadeOutAnimator)
        animatorSet.doOnStart {
            played = true
            view.visibility = View.VISIBLE
        }
        animatorSet.doOnEnd {
            view.visibility = View.GONE
        }
        animatorSet.start()
    }

    fun setupViews() {
        binding.vp.adapter = ThoughtPartPagerAdapter(this, thoughtRecord())
        binding.vp.offscreenPageLimit = 10 // TODO: Proper caching and page restoring method instead of keeping all views inflated.
        binding.progressBoxes.setViewPager(binding.vp)
        binding.vp.setPageTransformer(false, FadeParralaxTransformer())
        binding.vp.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                if (position == cognitiveDistortionPositionInViewPager && !played) {
                    showTooltip()
                }
            }
        })
    }

    fun onThoughtPartUpdated(thoughtPart: ThoughtPart) {
        thoughtRecord().thoughtParts[binding.vp.currentItem] = thoughtPart
    }

    private fun vm() = (activity as MainActivity).vm
    private fun thoughtRecord() = vm().thoughtRecord.value!!

}