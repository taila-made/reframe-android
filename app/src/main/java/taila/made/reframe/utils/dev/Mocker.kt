package taila.made.reframe.utils.dev

import taila.made.reframe.App
import taila.made.reframe.R
import taila.made.reframe.thoughts.ThoughtPart
import taila.made.reframe.thoughts.ThoughtRecord

object Mocker {

    private fun ctx() = App.component.context()

    fun createStubThoughtPart(title: String = "Identify Trigger",
                              question: String = "What Happened?",
                              subquestion: String = "Why? How? When? Where & and with who?",
                              answer: String? = null,
                              value: Int? = null) = ThoughtPart(title, question, subquestion, answer, value)

    fun createStubThoughtPartList(num: Int = 8): MutableList<ThoughtPart> {
        val result = mutableListOf<ThoughtPart>()
        for (i in 0..num) {
            result.add(createStubThoughtPart())
        }
        return result
    }

    fun createStubThoughtRecord() = ThoughtRecord(thoughtParts = createStubThoughtPartList(7))

    fun createDefaultThoughtRecord(): ThoughtRecord {
        val thoughtParts = mutableListOf<ThoughtPart>()
        thoughtParts.add(ThoughtPart(title = "Identify Trigger", question = "What happened?",
                subquestion = "Why, How, When, Where & With Who?"))
        thoughtParts.add(ThoughtPart(title = "Feelings and Emotions", question = "What are you feeling?",
                subquestion = "What emotion did I feel at that time? What else? How intense was it? Think about where you felt it your body and how strong it was."))
        thoughtParts.add(ThoughtPart(title = "Unhelpful Thoughts / Images", question = "Where's your head?",
                subquestion = "What went through my mind? What disturbed me? What did those houghts/images/memories mean to me, " +
                        "or say about me or the situation? What am I responding to? What ‘button’ is this pressing for me? What would be the worst thing about that, or that could happen?"))
        thoughtParts.add(ThoughtPart(title = "Cognitive Distortions", question = "Let's reflect!",
                subquestion = "Select all the cognitive distortions that apply."))
        thoughtParts.add(ThoughtPart(title = "Fact check #1", question = ctx().getString(R.string.thought_part_facts_supporting),
                subquestion = "What are the facts? What facts do I have that the unhelpful thought/s are totally true?"))
        thoughtParts.add(ThoughtPart(title = "Fact check #2", question = "Facts that provide evidence against the unhelpful thought",
                subquestion = ctx().getString(R.string.thought_part_facts_against)))
        thoughtParts.add(ThoughtPart(title = "Being realistic", question = "Alternative, more realistic and balanced perspective",
                subquestion = "STOP! Take a breath… What would someone else say about this situation? What’s the bigger picture? " +
                        "Is there another way of seeing it? What advice would I give someone else? Is my reaction in proportion to " +
                        "the actual event? Is this really as important as it seems?"))
        thoughtParts.add(ThoughtPart(title = "New Feelings and Emotions", question = "What are you feeling now?",
                subquestion = "Choose all that apply"))
        thoughtParts.add(ThoughtPart(title = "Better outlook", question = "What's are your thoughts now?",
                subquestion = "What could I do differently? What would be more effective? Do what works! Act wisely. " +
                        "What will be most helpful for me or the situation? What will the consequences be?"))

        return ThoughtRecord(thoughtParts)
    }

}