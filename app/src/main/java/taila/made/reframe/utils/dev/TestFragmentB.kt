package taila.made.reframe.utils.dev

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.transition.Fade
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.transition.doOnStart
import androidx.core.view.doOnPreDraw
import taila.made.reframe.R
import taila.made.reframe.databinding.FragmentTestBBinding

class TestFragmentB : Fragment() {

    companion object {
        val TAG = "TT:TestFragmentB"
    }

    lateinit var binding: FragmentTestBBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_test_b, container, false)

        setupTransitions()

        return binding.root
    }

    fun setupTransitions() {
        val sharedElementCardViewTransition = TransitionInflater.from(context)
                .inflateTransition(R.transition.shared_element_transition_chip_to_card_view)
        sharedElementEnterTransition = sharedElementCardViewTransition
        sharedElementReturnTransition = sharedElementCardViewTransition
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postponeEnterTransition()
        binding.chip.doOnPreDraw {
            startPostponedEnterTransition()
        }
    }

}