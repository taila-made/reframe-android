package taila.made.reframe.utils.dev

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.transition.Slide
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import taila.made.reframe.R
import taila.made.reframe.databinding.FragmentTestBinding

class TestFragment : Fragment() {

    lateinit var binding: FragmentTestBinding

    companion object {
        val TAG = "TT:TestFragment"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_test, container, false)

        binding.root.setOnClickListener { it ->
            fragmentManager!!.beginTransaction()
                    .setReorderingAllowed(true)
                    .addSharedElement(binding.chip, ViewCompat.getTransitionName(binding.chip)!!)
                    .replace(R.id.navHostFragment, TestFragmentB())
                    .addToBackStack(null)
                    .commit()
        }

        val mReEnterTransition = Slide(Gravity.START)
        mReEnterTransition.addTarget(binding.secondView)
        mReEnterTransition.excludeTarget(binding.chip, true)
        reenterTransition = mReEnterTransition
        exitTransition = mReEnterTransition

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}