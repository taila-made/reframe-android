package taila.made.reframe.utils

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import io.reactivex.Single
import io.reactivex.SingleEmitter
import taila.made.reframe.App
import taila.made.reframe.auth.AuthActivity
import taila.made.reframe.startActivityAndClearStack
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FirebaseUtil {

    companion object {
        const val TAG = "TT:FbaseUtl"
        const val COLLECTION_DEFAULT_THOUGHT_RECORD = "DefaultThoughtRecord"
    }

    @Inject
    lateinit var firebaseAuth: FirebaseAuth

    @Inject
    lateinit var firestore: FirebaseFirestore

    init {
        App.component.inject(this)
    }

    fun userId() = firebaseAuth.currentUser!!.uid

    fun isLoggedIn() = firebaseAuth.currentUser != null

    fun rxCreateUserWithEmailAndPassword(email: String, password: String): Single<AuthResult> {
        return Single.create { emitter ->
            if (email.isEmpty() or password.isEmpty()) {
                val e = Exception("Email and Password must be entered")
                emitter.onError(e)
            } else {
                firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        // Sign up success (NOTE THIS ALSO SIGNS IN)
                        Log.d(TAG, "createUserWithEmailAndPassword: SUCCESS")
                        emitter.onSuccess(task.result)
                    } else {
                        // Sign up failed
                        Log.w(TAG, "createUserWithEmailAndPassword: FAILURE")
                        emitter.onError(task.exception!!)
                    }
                }
            }
        }
    }

    fun rxSignInWithEmailAndPassword(email: String, password: String): Single<AuthResult> {
        return Single.create { emitter ->
            // TODO: Add more validation
            if (email.isEmpty() or password.isEmpty()) {
                val e = Exception("Email and Password must be entered")
                emitter.onError(e)
            } else {
                firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        // Sign in success
                        Log.d(TAG, "signInWithEmailAndPassword: SUCCESS")
                        emitter.onSuccess(task.result)
                    } else {
                        // Sign up failed
                        Log.w(TAG, "signInWithEmailAndPassword: FAILURE")
                        emitter.onError(task.exception!!)
                    }
                }
            }
        }
    }

    fun logout(activity: Activity) {
        firebaseAuth.signOut()
        activity.startActivityAndClearStack(Intent(activity, AuthActivity::class.java))
        activity.finish()
    }

    fun rxAddToCollection(collection: String, docName: String, document: Any): Single<Unit> {
        return Single.create { e: SingleEmitter<Unit> ->
            firestore.collection(collection).document(docName)
                    .set(document)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            e.onSuccess(Unit)
                        } else {
                            if (task.exception != null) {
                                e.onError(task.exception!!)
                            }
                        }
                    }
        }
    }

    fun rxGetCollection(collectionRef: CollectionReference): Single<QuerySnapshot> {
        return Single.create { e ->
            collectionRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    e.onSuccess(task.result)
                } else {
                    if (task.exception != null) {
                        e.onError(task.exception!!)
                    }
                }
            }
        }
    }

    fun rxGetCollection(documentRef: DocumentReference? = null, collectionPath: String): Single<QuerySnapshot> {
        val collectionRef = documentRef?.collection(collectionPath) ?: firestore.collection(collectionPath)
        return rxGetCollection(collectionRef)
    }

    fun rxGetDocument(collectionRef: CollectionReference? = null, documentPath: String): Single<DocumentSnapshot> {
        val docRef = collectionRef?.document(documentPath) ?: firestore.document(documentPath)
        return rxGetDocument(docRef)
    }

    fun rxGetDocument(documentRef: DocumentReference): Single<DocumentSnapshot> {
        return Single.create { e ->
            documentRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    e.onSuccess(task.result)
                } else {
                    if (task.exception != null) {
                        e.onError(task.exception!!)
                    }
                }
            }
        }
    }

}