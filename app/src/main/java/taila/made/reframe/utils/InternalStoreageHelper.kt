package taila.made.reframe.utils

import android.content.Context
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

object InternalStoreageHelper {

    @Throws(IOException::class)
    fun writeObject(context: Context, key: String, objekt: Any) {
        val fos = context.openFileOutput(key, Context.MODE_PRIVATE)
        val oos = ObjectOutputStream(fos)
        oos.writeObject(objekt)
        oos.close()
        fos.close()
    }

    @Throws(IOException::class, ClassNotFoundException::class)
    fun readObject(context: Context, key: String): Any? {
        val fis = context.openFileInput(key)
        val ois = ObjectInputStream(fis)
        return ois.readObject()
    }

}