package taila.made.reframe.utils.properties

/**
 * A delegate for creating a {@link Property} of <code>int</code> type.
 */
abstract class IntProp<T>(val name: String) {

    abstract operator fun set(`object`: T, value: Int)
    abstract operator fun get(`object`: T): Int

}