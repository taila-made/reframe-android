package taila.made.reframe.utils.dev

import android.util.Log
import taila.made.reframe.App
import taila.made.reframe.utils.FirebaseUtil
import javax.inject.Inject

class FirebaseHelper {

    @Inject
    lateinit var firebaseUtil: FirebaseUtil

    companion object {
        const val TAG = "TT:FirebaseHelper"
    }

    init {
        App.component.inject(this)
    }

    fun storeDefaultThoughtRecord(name: String) {
        firebaseUtil.rxAddToCollection(
                collection = FirebaseUtil.COLLECTION_DEFAULT_THOUGHT_RECORD,
                document = Mocker.createDefaultThoughtRecord(),
                docName = name
        ).subscribe({
            Log.d(TAG, "SUCCESS: Default Thought record stored.")
        }, {
            Log.d(TAG, it.localizedMessage)
            it.printStackTrace()
        })
    }

}