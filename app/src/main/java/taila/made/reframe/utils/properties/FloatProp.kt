package taila.made.reframe.utils.properties

/**
 * A delegate for creating a {@link Property} of <code>float</code> type.
 */
abstract class FloatProp<T>(val name: String) {

    abstract operator fun set(`object`: T, value: Float)
    abstract operator fun get(`object`: T): Float

}
