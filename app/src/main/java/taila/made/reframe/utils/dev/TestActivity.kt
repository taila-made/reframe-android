package taila.made.reframe.utils.dev

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import taila.made.reframe.R
import taila.made.reframe.databinding.LayoutFragmentHolderBinding


class TestActivity : AppCompatActivity() {

    companion object {
        const val TAG = "TT:TestAct"
    }

    lateinit var binding: LayoutFragmentHolderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.layout_fragment_holder)
    }


}