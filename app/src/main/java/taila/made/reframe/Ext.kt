package taila.made.reframe

import android.app.Activity
import android.content.Intent

fun Activity.startActivityAndClearStack(intent: Intent) {
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    startActivity(intent)
}
