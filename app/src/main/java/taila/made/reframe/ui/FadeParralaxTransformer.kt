package taila.made.reframe.ui

import android.support.v4.view.ViewPager
import android.view.View
import android.widget.TextView
import taila.made.reframe.R

class FadeParralaxTransformer() : ViewPager.PageTransformer {

    override fun transformPage(pageV: View, position: Float) {
        when {
            position <= 1 -> { // [-1, 1]
                pageV.findViewById<TextView>(R.id.tvThoughtRecordPartTitle).translationX = scaleTranslationX(scaleFactor = 0.9f, position = position, pageViewWidth = pageV.width)
                pageV.findViewById<TextView>(R.id.tvBigQuestion).translationX = scaleTranslationX(scaleFactor = 0.2f, position = position, pageViewWidth = pageV.width)
                pageV.findViewById<TextView>(R.id.tvSubQuestion).translationX = scaleTranslationX(scaleFactor = 0.6f, position = position, pageViewWidth = pageV.width)

                pageV.findViewById<TextView>(R.id.tvThoughtRecordPartTitle).alpha = scaleAlpha(scaleFactor = 2.5f, position = position)
                pageV.findViewById<TextView>(R.id.tvBigQuestion).alpha = scaleAlpha(scaleFactor = 0.5f, position = position)
                pageV.findViewById<TextView>(R.id.tvSubQuestion).alpha = scaleAlpha(scaleFactor = 0.8f, position = position)
            }
        }
    }

    private fun scaleTranslationX(scaleFactor: Float, pageViewWidth: Int, position: Float): Float {
        return position * pageViewWidth * scaleFactor
    }

    private fun scaleAlpha(scaleFactor: Float = 1f, position: Float): Float {
        return 1 - (scaleFactor * Math.abs(position))
    }

}