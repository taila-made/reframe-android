package taila.made.reframe.ui

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import android.widget.TextView

object AnimationUtil {

    // CONSTANTS ////////////////////////////////////////////////////
    const val DIRECTION_LEFT = 0
    const val DIRECTION_RIGHT = 1
    const val DIRECTION_UP = 2
    const val DIRECTION_DOWN = 3

    const val DURATION_MOBILE_TYPICAL = 300L
    const val DURATION_MOBILE_ENTERING = 225L //
    const val DURATION_MOBILE_EXITING = 195L // For elements leaving the screen
    const val DURATION_MOBILE_COMPLEX = 375L // For large, full screen transitions

    // INTERPOLATORS ////////////////////////////////////////////////////
    private var fastOutSlowIn: Interpolator? = null
    private var fastOutLinearIn: Interpolator? = null
    private var linearOutSlowIn: Interpolator? = null
    private var linear: Interpolator? = null

    fun getFastOutSlowInInterpolator(context: Context): Interpolator {
        if (fastOutSlowIn == null) {
            fastOutSlowIn = AnimationUtils.loadInterpolator(context,
                    android.R.interpolator.fast_out_slow_in)
        }
        return fastOutSlowIn!!
    }

    fun getFastOutLinearInInterpolator(context: Context): Interpolator {
        if (fastOutLinearIn == null) {
            fastOutLinearIn = AnimationUtils.loadInterpolator(context,
                    android.R.interpolator.fast_out_linear_in)
        }
        return fastOutLinearIn!!
    }

    fun getLinearOutSlowInInterpolator(context: Context): Interpolator {
        if (linearOutSlowIn == null) {
            linearOutSlowIn = AnimationUtils.loadInterpolator(context,
                    android.R.interpolator.linear_out_slow_in)
        }
        return linearOutSlowIn!!
    }

    fun getLinearInterpolator(): Interpolator {
        if (linear == null) {
            linear = LinearInterpolator()
        }
        return linear!!
    }

    // PROPERTY ANIMATORS ////////////////////////////////////////////////////
    fun popAnimation(view: View, popMax: Float = 1.2f, duration: Long = AnimationUtil.DURATION_MOBILE_TYPICAL): ValueAnimator {
        return ValueAnimator.ofFloat(1f, popMax, 1f).apply {
            this.duration = duration
            addUpdateListener {
                view.scaleX = animatedValue as Float
                view.scaleY = animatedValue as Float
            }
        }
    }

    fun translateX(view: View, from: Float, to: Float, duration: Long = DURATION_MOBILE_TYPICAL): ValueAnimator {
        return ValueAnimator.ofFloat(from, to).apply {
            this.duration = duration
            addUpdateListener { animator -> view.translationX = animator.animatedValue as Float }
        }
    }

    fun translateY(view: View, from: Float, to: Float, duration: Long = DURATION_MOBILE_TYPICAL): ValueAnimator {
        return ValueAnimator.ofFloat(from, to).apply {
            this.duration = duration
            addUpdateListener { animator -> view.translationY = animator.animatedValue as Float }
        }
    }

    fun textColorAnimator(tv: TextView, colorFrom: Int, colorTo: Int, duration: Long = DURATION_MOBILE_TYPICAL): ValueAnimator {
        return ValueAnimator.ofArgb(colorFrom, colorTo).apply {
            this.duration = duration
            addUpdateListener { it -> tv.setTextColor(it.animatedValue as Int) }
        }
    }

    fun scaleY(view: View, from: Float, to: Float = 1f, duration: Long = DURATION_MOBILE_TYPICAL): ValueAnimator {
        return ValueAnimator.ofFloat(from, to).apply {
            this.duration = duration
            this.addUpdateListener { animator ->
                view.scaleY = animator.animatedValue as Float
            }
        }
    }

    fun scaleX(view: View, from: Float, to: Float = 1f, duration: Long = DURATION_MOBILE_TYPICAL): ValueAnimator {
        return ValueAnimator.ofFloat(from, to).apply {
            this.duration = duration
            this.addUpdateListener { animator -> view.scaleX = animator.animatedValue as Float }
        }
    }

    fun fade(view: View, from: Float = 0f, to: Float = 1f, duration: Long = DURATION_MOBILE_TYPICAL): ValueAnimator {
        return ValueAnimator.ofFloat(from, to).apply {
            this.duration = duration
            addUpdateListener { it -> view.alpha = it.animatedValue as Float }
        }
    }

    fun creepIn(view: View, direction: Int, translationAmountInDp: Int, duration: Long = DURATION_MOBILE_TYPICAL): AnimatorSet {
        var translationAmount = UIUtil.pxFromDp(translationAmountInDp.toFloat())
        if (direction == DIRECTION_DOWN || direction == DIRECTION_RIGHT) {
            translationAmount = -translationAmount
        }

        val translateAnimator = if (direction == DIRECTION_RIGHT || direction == DIRECTION_LEFT) {
            translateX(view = view, from = translationAmount, to = 0f, duration = duration)
        } else {
            translateY(view = view, from = translationAmount, to = 0f, duration = duration)
        }

        val alphaAnimator = fade(view, from = 0f, to = 1f, duration = duration)
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(translateAnimator, alphaAnimator)
        animatorSet.duration = duration
        return animatorSet
    }

}

