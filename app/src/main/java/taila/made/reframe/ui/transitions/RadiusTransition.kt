package taila.made.reframe.ui.transitions

import android.animation.Animator
import android.annotation.TargetApi
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.ColorInt
import android.support.design.button.MaterialButton
import android.support.design.card.MaterialCardView
import android.support.design.chip.Chip
import android.support.transition.Transition
import android.support.transition.TransitionValues
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import taila.made.reframe.R
import taila.made.reframe.ui.UIUtil
import taila.made.reframe.ui.cornerRadiusAnimator

/**
 * Transition for animating background color
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
class RadiusTransition : Transition {

    companion object {
        private const val TAG = "TT:UI:BgColorTrans"
        private const val PROPNAME_RADIUS = "taila.made.reframe.ui:RadiusTransition:radius"
        private val TRANSITION_PROPERTIES = arrayOf(PROPNAME_RADIUS)


        fun makeExtrasBundle(startRadius: Float): Bundle {
            return Bundle().apply { putFloat(PROPNAME_RADIUS, startRadius) }
        }

        fun makeExtrasBundle(view: View): Bundle {
            return when (view) {
                is Chip -> {
                    Bundle().apply {
                        putFloat(PROPNAME_RADIUS, view.chipCornerRadius)
                    }
                }
                is MaterialCardView -> {
                    Bundle().apply {
                        putFloat(PROPNAME_RADIUS, view.radius)
                    }
                }
                is MaterialButton -> {
                    Bundle().apply {
                        putFloat(PROPNAME_RADIUS, view.cornerRadius.toFloat())
                    }
                }
                else -> {
                    when (view.background) {
                        is GradientDrawable -> {
                            Bundle().apply {
                                putFloat(PROPNAME_RADIUS, (view.background as GradientDrawable).cornerRadius)
                            }
                        }
                        else -> {
                            Bundle()
                        }
                    }
                }
            }
        }
    }

    constructor() {}

    @TargetApi(Build.VERSION_CODES.KITKAT)
    constructor(startValuesExtras: Bundle? = null) {
        this.startValuesExtras = startValuesExtras
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    constructor(startRadius: Float, endRadius: Float) {
        this.startRadius = startRadius
        this.endRadius = endRadius
    }

    var startValuesExtras: Bundle? = null

    var startRadius: Float = 0f
    @ColorInt
    var endRadius: Float = UIUtil.pxFromDp(8f)

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.Radius)
        startRadius = ta.getDimensionPixelSize(R.styleable.Radius_startRadius, 0).toFloat()
        endRadius = ta.getDimensionPixelSize(R.styleable.Radius_endRadius, 8).toFloat()
        ta.recycle()
    }

    override fun captureStartValues(transitionValues: TransitionValues) {
        if (startValuesExtras != null) {
            transitionValues.values[PROPNAME_RADIUS] = startValuesExtras!![PROPNAME_RADIUS]
        } else {
            val view = transitionValues.view
            try {
                when (view) {
                    is Chip -> {
                        transitionValues.values[PROPNAME_RADIUS] = view.chipCornerRadius
                    }
                    is MaterialCardView -> {
                        transitionValues.values[PROPNAME_RADIUS] = view.radius
                    }
                    is MaterialButton -> {
                        transitionValues.values[PROPNAME_RADIUS] = view.cornerRadius.toFloat()
                    }
                    else -> {
                        transitionValues.values[PROPNAME_RADIUS] = (view.background as GradientDrawable).cornerRadius
                    }
                }
            } catch (e: Exception) {
                Log.d(TAG, e.localizedMessage)
                transitionValues.values[PROPNAME_RADIUS] = startRadius
            }
        }
    }

    override fun captureEndValues(transitionValues: TransitionValues) {
        val view = transitionValues.view
        try {
            when (view) {
                is Chip -> {
                    transitionValues.values[PROPNAME_RADIUS] = view.chipCornerRadius
                }
                is MaterialCardView -> {
                    transitionValues.values[PROPNAME_RADIUS] = view.radius
                }
                is MaterialButton -> {
                    transitionValues.values[PROPNAME_RADIUS] = view.cornerRadius.toFloat()
                }
                else -> {
                    transitionValues.values[PROPNAME_RADIUS] = (view.background as GradientDrawable).cornerRadius
                }
            }
        } catch (e: Exception) {
            Log.d(TAG, e.localizedMessage)
            transitionValues.values[PROPNAME_RADIUS] = endRadius
        }
    }

    override fun createAnimator(sceneRoot: ViewGroup, startValues: TransitionValues?, endValues: TransitionValues?): Animator? {
        Log.d(TAG, "createAnimator()")
        if (startValues == null || endValues == null) {
            return null
        }

        startRadius = startValues.values[PROPNAME_RADIUS] as Float
        endRadius = endValues.values[PROPNAME_RADIUS] as Float

        val view = endValues.view

        return when (view) {
            is Chip -> {
                view.cornerRadiusAnimator(floatArrayOf(startRadius, endRadius))
            }
            is MaterialCardView -> {
                view.cornerRadiusAnimator(floatArrayOf(startRadius, endRadius))
            }
            is MaterialButton -> {
                view.cornerRadiusAnimator(floatArrayOf(startRadius, endRadius))
            }
            else -> {
                null
            }
        }
    }

}