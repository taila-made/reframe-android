package taila.made.reframe.ui

import android.graphics.*
import android.graphics.drawable.Drawable
import android.support.annotation.ColorInt
import taila.made.reframe.utils.properties.FloatProp
import taila.made.reframe.utils.properties.IntProp
import taila.made.reframe.utils.properties.PropertyFactory

/**
 * A drawable that can morph size, shape (via it's corner radius) and color.  Specifically this is
 * useful for animating between a FAB and a dialog.
 */
class MorphDrawable(@ColorInt color: Int, private var cornerRadius: Float) : Drawable() {

    private val paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    var color: Int
        get() = paint.color
        set(@ColorInt color) {
            paint.color = color
            invalidateSelf()
        }

    init {
        paint.color = color
    }

    internal fun getCornerRadius(): Float {
        return cornerRadius
    }

    internal fun setCornerRadius(cornerRadius: Float) {
        this.cornerRadius = cornerRadius
        invalidateSelf()
    }

    override fun draw(canvas: Canvas) {
        canvas.drawRoundRect(
                bounds.left.toFloat(),
                bounds.top.toFloat(),
                bounds.right.toFloat(),
                bounds.bottom.toFloat(),
                cornerRadius,
                cornerRadius,
                paint)
    }

    override fun getOutline(outline: Outline) {
        outline.setRoundRect(bounds, cornerRadius)
    }

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
        invalidateSelf()
    }

    override fun setColorFilter(cf: ColorFilter?) {
        paint.colorFilter = cf
        invalidateSelf()
    }

    override fun getOpacity(): Int {
        return if (paint.alpha == 255) PixelFormat.OPAQUE else PixelFormat.TRANSLUCENT
    }

    companion object {
        val CORNER_RADIUS = PropertyFactory.createFloatProperty(object : FloatProp<MorphDrawable>("cornerRadius") {
            override operator fun set(morphDrawable: MorphDrawable, value: Float) {
                morphDrawable.setCornerRadius(value)
            }

            override operator fun get(morphDrawable: MorphDrawable): Float {
                return morphDrawable.getCornerRadius()
            }
        })

        val COLOR = PropertyFactory.createIntProperty(object : IntProp<MorphDrawable>("color") {
            override operator fun set(morphDrawable: MorphDrawable, value: Int) {
                morphDrawable.color = value
            }

            override operator fun get(morphDrawable: MorphDrawable): Int {
                return morphDrawable.color
            }
        })
    }

}