package taila.made.reframe.ui

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.support.annotation.ColorRes
import android.support.annotation.DimenRes
import android.support.annotation.DrawableRes
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.View
import taila.made.reframe.App
import java.util.*
import kotlin.math.roundToInt

object UIUtil {

    private fun ctx() = App.component.context()

    fun gradientDrawableRect(cornerRadius: Float = 0f, @ColorRes startColor: Int, @ColorRes endColor: Int): Drawable {
        val drawable = GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                intArrayOf(startColor, endColor))
        drawable.shape = GradientDrawable.RECTANGLE
        drawable.cornerRadius = cornerRadius
        return drawable
    }

    fun dpFromPx(px: Float, context: Context): Float {
        return px / (context.resources.displayMetrics.densityDpi / 160)
    }

    fun pxFromDp(dp: Float, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi / 160)
    }

    fun roundedPxFromDp(dp: Float, context: Context): Int {
        return pxFromDp(dp, context).roundToInt()
    }

    fun getStatusBarHeight(context: Context): Int {
        var result = 0
        val resourceId =
                context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = context.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun addMargin(
            view: View,
            marginSide: Int,
            marginAmountInDp: Int = 0,
            marginAmountInPx: Int = 0
    ) {
        val lp = view.layoutParams as ConstraintLayout.LayoutParams
        val marginInPx = if (marginAmountInPx == 0) {
            pxFromDp(marginAmountInDp.toFloat(), view.context).toInt()
        } else {
            marginAmountInPx
        }

        when (marginSide) {
            Gravity.START -> lp.leftMargin += marginInPx
            Gravity.TOP -> lp.topMargin += marginInPx
            Gravity.END -> lp.rightMargin += marginInPx
            Gravity.BOTTOM -> lp.bottomMargin += marginInPx
        }
        view.layoutParams = lp
    }

    fun addStatusBarMargin(view: View) {
        addMargin(view, marginSide = Gravity.TOP, marginAmountInPx = getStatusBarHeight(view.context))
    }

    fun addStatusBarMargin(vararg views: View) {
        for (view in views) addStatusBarMargin(view)
    }

    fun randomColor(): Int {
        val rnd = Random()
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
    }


    fun chipGroupHeight(@DimenRes chipHeightDimenRes: Int, @DimenRes chipGroupVerticalSpacingDimenRes: Int): Int {
        return ctx().resources.getDimensionPixelSize(chipHeightDimenRes) + ctx().resources.getDimensionPixelSize(chipGroupVerticalSpacingDimenRes)
    }

    fun getDrawable(@DrawableRes resId: Int): Drawable {
        return ctx().getDrawable(resId)
    }

    fun getColor(resId: Int): Int {
        return ContextCompat.getColor(ctx(), resId)
    }

    fun dpFromPx(px: Float): Float {
        return px / (ctx().resources.displayMetrics.densityDpi / 160)
    }

    fun pxFromDp(dp: Float): Float {
        return dp * (ctx().resources.displayMetrics.densityDpi / 160)
    }

    fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }

    fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    fun gradientAnimation(from: GradientDrawable,
                          fromDuration: Int = AnimationUtil.DURATION_MOBILE_TYPICAL.toInt() / 2,
                          to: GradientDrawable,
                          toDuration: Int = AnimationUtil.DURATION_MOBILE_TYPICAL.toInt() / 2,
                          exitDuration: Int = AnimationUtil.DURATION_MOBILE_TYPICAL.toInt(),
                          enterDuration: Int = AnimationUtil.DURATION_MOBILE_TYPICAL.toInt() / 2 * 3): AnimationDrawable {
        val animationDrawable = AnimationDrawable()
        animationDrawable.addFrame(from, fromDuration)
        animationDrawable.addFrame(to, toDuration)

        animationDrawable.setExitFadeDuration(exitDuration)
        animationDrawable.setEnterFadeDuration(enterDuration)
        animationDrawable.isOneShot = true

        return animationDrawable
    }

}