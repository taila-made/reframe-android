package taila.made.reframe.ui.transitions

import android.support.transition.*

val MATERIAL_UI_EXT = "TT:UI"

// ******** CONVENIENCE ******** //
fun TransitionSet.configure(transitionsToAdd: Array<Transition> = emptyArray(),
                            pathMotion: PathMotion? = null,
                            targetIds: IntArray = intArrayOf()): TransitionSet {
    return apply {
        addTransitions(transitionsToAdd)
        addTargets(targetIds)
        if (pathMotion != null) {
            setPathMotion(ArcMotion())
        }
    }
}

fun TransitionSet.addTransitions(transitions: Array<Transition> = emptyArray()) {
    for (transition in transitions) {
        addTransition(transition)
    }
}

fun Transition.addTargets(targetIds: IntArray = intArrayOf()) {
    for (targetId in targetIds) {
        addTarget(targetId)
    }
}

fun Transition.excludeTargets(targetIds: IntArray = intArrayOf()) {
    for (targetId in targetIds) {
        excludeTarget(targetId, true)
    }
}

// ******** COMMON ******** //
fun TransitionSet.buildMorphTransition(targetIds: IntArray = intArrayOf(),
                                       pathMotion: PathMotion = ArcMotion()): TransitionSet {
    return configure(
            transitionsToAdd = arrayOf(RadiusTransition(),
                    BackgroundColorChangeTransition(),
                    ChangeBounds(),
                    ElevationTransition()),
            pathMotion = pathMotion,
            targetIds = targetIds)
}