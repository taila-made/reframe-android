package taila.made.reframe.ui

import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.res.ColorStateList
import android.support.design.button.MaterialButton
import android.support.design.card.MaterialCardView
import android.support.design.chip.Chip
import android.support.v4.content.ContextCompat
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import kotlin.math.roundToInt

val MATERIAL_UI_EXT = "TT:UI"

// TODO: MAKE SINGLE and DOUBLE VARIABLE VERSION OF ALL THESE FUNCTIONS (To avoid array list creation overhead)

// ******** VIEW ******** //
fun View.elevationAnimator(to: FloatArray): ValueAnimator {
    return ValueAnimator.ofFloat(*to).apply {
        addUpdateListener {
            elevation = animatedValue as Float
        }
    }
}


// ******** CHIP ******** //
fun Chip.bgColorAnimator(toColors: IntArray): ValueAnimator {
    return ValueAnimator.ofArgb(*toColors).apply {
        addUpdateListener {
            chipBackgroundColor = ColorStateList.valueOf(animatedValue as Int)
        }
    }
}

fun Chip.cornerRadiusAnimator(toRadius: FloatArray): ValueAnimator {
    return ValueAnimator.ofFloat(*toRadius).apply {
        addUpdateListener {
            chipCornerRadius = animatedValue as Float
        }
    }
}

fun Chip.textColorAnimator(to: IntArray): ValueAnimator {
    return ValueAnimator.ofArgb(*to).apply {
        addUpdateListener {
            setTextColor(ColorStateList.valueOf(animatedValue as Int))
        }
    }
}

fun Chip.popAnimation(
        fromBgColor: Int = chipBackgroundColor!!.defaultColor,
        toBgColor: Int
): AnimatorSet {
    // TODO: For some reason text color change doesn't work
    val bgColorAnimator = bgColorAnimator(intArrayOf(fromBgColor, toBgColor))
    val scaleAnimator = AnimationUtil.popAnimation(this, popMax = 1.1f)
    return AnimatorSet().apply {
        //        playTogether(bgColorAnimator, scaleAnimator, textColorAnimator)
        playTogether(bgColorAnimator, scaleAnimator)
    }
}

// ******** MATERIALBUTTON ******** //
// TODO: This needs to be more flexible (must be either something wrong or a better way to do this)
fun MaterialButton.shrinkTextAnimation(): AnimatorSet {
    val fadeTextAnimator = AnimationUtil
            .textColorAnimator(
                    tv = this,
                    colorFrom = currentTextColor,
                    colorTo = ContextCompat.getColor(context, android.R.color.transparent)
            )

    val textScaleAnimator = ValueAnimator.ofFloat(textSize, 0f).apply {
        addUpdateListener {
            setTextSize(TypedValue.COMPLEX_UNIT_PX, animatedValue as Float)
        }
    }
    val animatedPaddingEnd = ValueAnimator.ofInt(buttonPaddingEnd, UIUtil.pxFromDp(5.5f, context).roundToInt()).apply {
        addUpdateListener {
            setButtonPadding(buttonPaddingStart, buttonPaddingTop, animatedValue as Int, buttonPaddingBottom)
        }
    }
    return AnimatorSet().apply {
        playTogether(fadeTextAnimator, textScaleAnimator, animatedPaddingEnd)
    }
}

fun MaterialButton.cornerRadiusAnimator(toRadius: IntArray): ValueAnimator {
    return ValueAnimator.ofInt(*toRadius).apply {
        addUpdateListener {
            cornerRadius = animatedValue as Int
        }
    }
}

fun MaterialButton.cornerRadiusAnimator(toRadius: FloatArray): ValueAnimator {
    return ValueAnimator.ofFloat(*toRadius).apply {
        addUpdateListener {
            cornerRadius = (animatedValue as Float).roundToInt()
        }
    }
}

fun MaterialButton.bgColorAnimator(toColors: IntArray): ValueAnimator {
    return ValueAnimator.ofArgb(*toColors).apply {
        addUpdateListener {
            backgroundTintList = ColorStateList.valueOf(animatedValue as Int)
        }
    }
}

// ******** MATERIALCARDVIEW ******** //
fun MaterialCardView.bgColorAnimator(toColors: IntArray): ValueAnimator {
    return ValueAnimator.ofArgb(*toColors).apply {
        addUpdateListener {
            setCardBackgroundColor(animatedValue as Int)
        }
    }
}

fun MaterialCardView.cornerRadiusAnimator(toRadius: FloatArray): ValueAnimator {
    return ValueAnimator.ofFloat(*toRadius).apply {
        addUpdateListener {
            radius = animatedValue as Float
        }
    }
}


fun MaterialCardView.elevationAnimator(to: FloatArray): ValueAnimator {
    return ValueAnimator.ofFloat(*to).apply {
        addUpdateListener {
            cardElevation = animatedValue as Float
        }
    }
}


// ******** TEXTVIEW ******** //
fun TextView.textColorAnimator(to: IntArray): ValueAnimator {
    return ValueAnimator.ofArgb(*to).apply {
        addUpdateListener {
            setTextColor(animatedValue as Int)
        }
    }
}
