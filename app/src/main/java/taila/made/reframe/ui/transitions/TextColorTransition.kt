package taila.made.reframe.ui.transitions

import android.animation.Animator
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.ColorInt
import android.support.transition.Transition
import android.support.transition.TransitionValues
import android.util.AttributeSet
import android.util.Log
import android.view.ViewGroup
import android.widget.TextView
import taila.made.reframe.R
import taila.made.reframe.ui.textColorAnimator

/**
 * Transition for animating background TextColor
 * TODO: Add support for Chip
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
class TextColorTransition : Transition {

    companion object {
        private const val TAG = "TT:UI:EtextColorTrans"
        private const val PROPNAME_TEXT_COLOR = "taila.made.reframe.ui:TextColorTransition:textColor"
        private val TRANSITION_PROPERTIES = arrayOf(PROPNAME_TEXT_COLOR)


        fun makeExtrasBundle(@ColorInt startTextColor: Int): Bundle {
            return Bundle().apply { putInt(PROPNAME_TEXT_COLOR, startTextColor) }
        }

        fun makeExtrasBundle(view: TextView): Bundle {
            return Bundle().apply {
                putInt(PROPNAME_TEXT_COLOR, view.currentTextColor)
            }
        }
    }

    var custom = false

    constructor() {}

    @TargetApi(Build.VERSION_CODES.KITKAT)
    constructor(startValuesExtras: Bundle? = null) {
        this.startValuesExtras = startValuesExtras
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    constructor(startTextColor: Int, endTextColor: Int) {
        this.startTextColor = startTextColor
        this.endTextColor = endTextColor
        custom == true
    }

    var startValuesExtras: Bundle? = null


    var startTextColor: Int = Color.WHITE
    var endTextColor: Int = Color.BLACK

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.textColor)
        startTextColor = ta.getColor(R.styleable.textColor_startTextColor, 0)
        endTextColor = ta.getColor(R.styleable.textColor_endTextColor, 8)
        ta.recycle()
    }

    override fun captureStartValues(transitionValues: TransitionValues) {
        if (custom) {
            transitionValues.values[PROPNAME_TEXT_COLOR] = startTextColor
        } else {
            if (startValuesExtras != null) {
                transitionValues.values[PROPNAME_TEXT_COLOR] = startValuesExtras!![PROPNAME_TEXT_COLOR]
            } else {
                try {
                    val view = transitionValues.view as TextView
                    transitionValues.values[PROPNAME_TEXT_COLOR] = view.currentTextColor
                } catch (e: Exception) {
                    Log.d(TAG, e.localizedMessage)
                    transitionValues.values[PROPNAME_TEXT_COLOR] = startTextColor
                }
            }
        }
    }

    override fun captureEndValues(transitionValues: TransitionValues) {
        if (custom) {
            transitionValues.values[PROPNAME_TEXT_COLOR] = endTextColor
        } else {
            try {
                val view = transitionValues.view as TextView
                transitionValues.values[PROPNAME_TEXT_COLOR] = view.currentTextColor
            } catch (e: Exception) {
                Log.d(TAG, e.localizedMessage)
                transitionValues.values[PROPNAME_TEXT_COLOR] = endTextColor
            }
        }
    }

    override fun createAnimator(sceneRoot: ViewGroup, startValues: TransitionValues?, endValues: TransitionValues?): Animator? {
        Log.d(TAG, "createAnimator()")
        if (startValues == null || endValues == null) {
            return null
        }

        startTextColor = startValues.values[PROPNAME_TEXT_COLOR] as Int
        endTextColor = endValues.values[PROPNAME_TEXT_COLOR] as Int

        return try {
            (endValues.view as TextView).textColorAnimator(intArrayOf(startTextColor, endTextColor))
        } catch (e: Exception) {
            Log.d(TAG, e.localizedMessage)
            null
        }
    }

}