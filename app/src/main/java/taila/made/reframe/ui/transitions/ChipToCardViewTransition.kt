package taila.made.reframe.ui.transitions

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.annotation.TargetApi
import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.support.design.card.MaterialCardView
import android.support.design.chip.Chip
import android.support.v4.content.ContextCompat
import android.transition.ChangeBounds
import android.transition.TransitionValues
import android.util.AttributeSet
import android.view.ViewGroup
import taila.made.reframe.R

/** This transition will move in an arc motion in addition to:
 * - Changing the background color
 * - Changing the corner radius
 * - Changing the elevation
 */
class ChipToCardViewTransition : ChangeBounds {

    companion object {
        private val PROPNAME_BG_COLOR = "taila.made.reframe.ui.transitions:chipToCardViewTransition:backgroundColor"
        private val PROPNAME_CORNER_RADIUS = "taila.made.reframe.ui.transitions:chipToCardViewTransition:radius"
        private val PROPNAME_ELEVATION = "taila.made.reframe.ui.transitions:chipToCardViewTransition:elevation"
        private val TRANSITION_PROPERTIES = arrayOf(PROPNAME_BG_COLOR, PROPNAME_CORNER_RADIUS, PROPNAME_ELEVATION)

        fun makeExtrasBundle(chip: Chip): Bundle {
            val extras = Bundle()
            extras.putInt(PROPNAME_BG_COLOR, chip.chipBackgroundColor!!.defaultColor)
            extras.putFloat(PROPNAME_CORNER_RADIUS, chip.chipCornerRadius)
            extras.putFloat(PROPNAME_ELEVATION, chip.elevation)
            return extras
        }
    }

    var startValues: Bundle? = null
    var endValues: Bundle? = null

    @TargetApi(Build.VERSION_CODES.KITKAT)
    constructor(startValues: Bundle? = null) {
        this.startValues = startValues
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
    }

    override fun getTransitionProperties(): Array<String>? {
        return TRANSITION_PROPERTIES
    }

    override fun captureStartValues(transitionValues: TransitionValues) {
        super.captureStartValues(transitionValues)
        if (startValues != null) {
            transitionValues.values[PROPNAME_BG_COLOR] = startValues!![PROPNAME_BG_COLOR]
            transitionValues.values[PROPNAME_CORNER_RADIUS] = startValues!![PROPNAME_CORNER_RADIUS]
            transitionValues.values[PROPNAME_ELEVATION] = startValues!![PROPNAME_ELEVATION]
        } else {
            when {
                transitionValues.view is Chip -> {
                    transitionValues.values[PROPNAME_BG_COLOR] = (transitionValues.view as Chip).chipBackgroundColor!!.defaultColor
                    transitionValues.values[PROPNAME_CORNER_RADIUS] = (transitionValues.view as Chip).chipCornerRadius
                    transitionValues.values[PROPNAME_ELEVATION] = (transitionValues.view as Chip).elevation
                }
                transitionValues.view is MaterialCardView -> {
                    transitionValues.values[PROPNAME_BG_COLOR] = (transitionValues.view as MaterialCardView).cardBackgroundColor.defaultColor
                    transitionValues.values[PROPNAME_CORNER_RADIUS] = (transitionValues.view as MaterialCardView).radius
                    transitionValues.values[PROPNAME_ELEVATION] = (transitionValues.view as MaterialCardView).cardElevation
                }
                else -> transitionValues.values[PROPNAME_BG_COLOR] = ContextCompat
                        .getColor(transitionValues.view.context, R.color.purple)
            }
        }
    }

    override fun captureEndValues(transitionValues: TransitionValues) {
        super.captureEndValues(transitionValues)
        if (endValues != null) {
            transitionValues.values[PROPNAME_BG_COLOR] = endValues!![PROPNAME_BG_COLOR]
            transitionValues.values[PROPNAME_CORNER_RADIUS] = endValues!![PROPNAME_CORNER_RADIUS]
            transitionValues.values[PROPNAME_ELEVATION] = endValues!![PROPNAME_ELEVATION]
        } else {
            when {
                transitionValues.view is Chip -> {
                    transitionValues.values[PROPNAME_BG_COLOR] = (transitionValues.view as Chip).chipBackgroundColor!!.defaultColor
                    transitionValues.values[PROPNAME_CORNER_RADIUS] = (transitionValues.view as Chip).chipCornerRadius
                    transitionValues.values[PROPNAME_ELEVATION] = (transitionValues.view as Chip).elevation
                }
                transitionValues.view is MaterialCardView -> {
                    transitionValues.values[PROPNAME_BG_COLOR] = (transitionValues.view as MaterialCardView).cardBackgroundColor.defaultColor
                    transitionValues.values[PROPNAME_CORNER_RADIUS] = (transitionValues.view as MaterialCardView).radius
                    transitionValues.values[PROPNAME_ELEVATION] = (transitionValues.view as MaterialCardView).cardElevation
                }
                else -> transitionValues.values[PROPNAME_BG_COLOR] = ContextCompat
                        .getColor(transitionValues.view.context, android.R.color.white)
            }
        }
    }

    override fun createAnimator(sceneRoot: ViewGroup, startValues: TransitionValues?, endValues: TransitionValues?): Animator? {
        val changeBounds = super.createAnimator(sceneRoot, startValues, endValues)
        if (startValues == null || endValues == null || changeBounds == null) {
            return null
        }
        val colorAnimator = makeColorAnimator(startValues, endValues)
        val cornerRadiusAnimator = makeCornerRadiusAnimator(startValues, endValues)
        val elevationAnimator = makeElevationAnimator(startValues, endValues)

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(changeBounds, colorAnimator, cornerRadiusAnimator, elevationAnimator)
        return animatorSet
    }

    private fun makeCornerRadiusAnimator(startValues: TransitionValues, endValues: TransitionValues): ValueAnimator? {
        return when {
            endValues.view is MaterialCardView -> {
                val cardView = endValues.view as MaterialCardView
                val startCornerRadius = startValues.values[PROPNAME_CORNER_RADIUS] as Float
                val endCornerRadius = endValues.values[PROPNAME_CORNER_RADIUS] as Float
                val animator = ValueAnimator.ofFloat(startCornerRadius, endCornerRadius)
                animator.addUpdateListener { it ->
                    cardView.radius = it.animatedValue as Float
                }
                animator
            }
            endValues.view is Chip -> {
                val chip = endValues.view as Chip
                val startCornerRadius = startValues.values[PROPNAME_CORNER_RADIUS] as Float
                val endCornerRadius = endValues.values[PROPNAME_CORNER_RADIUS] as Float
                val animator = ValueAnimator.ofFloat(startCornerRadius, endCornerRadius)
                animator.addUpdateListener { it ->
                    chip.chipCornerRadius = it.animatedValue as Float
                }
                animator
            }
            else -> null
        }
    }

    private fun makeElevationAnimator(startValues: TransitionValues, endValues: TransitionValues): ValueAnimator? {
        return when {
            endValues.view is MaterialCardView -> {
                val cardView = endValues.view as MaterialCardView
                val startElevation = startValues.values[PROPNAME_ELEVATION] as Float
                val endElevation = endValues.values[PROPNAME_ELEVATION] as Float
                val animator = ValueAnimator.ofFloat(startElevation, endElevation)
                animator.addUpdateListener { it ->
                    cardView.cardElevation = it.animatedValue as Float
                }
                animator
            }
            endValues.view is Chip -> {
                val chip = endValues.view as Chip
                val startElevation = startValues.values[PROPNAME_ELEVATION] as Float
                val endElevation = endValues.values[PROPNAME_ELEVATION] as Float
                val animator = ValueAnimator.ofFloat(startElevation, endElevation)
                animator.addUpdateListener { it ->
                    chip.elevation = it.animatedValue as Float
                }
                animator
            }
            else -> null
        }
    }

    private fun makeColorAnimator(startValues: TransitionValues, endValues: TransitionValues): ValueAnimator? {
        return when {
            endValues.view is MaterialCardView -> {
                val cardView = endValues.view as MaterialCardView
                val startColor = startValues.values[PROPNAME_BG_COLOR] as Int
                val endColor = endValues.values[PROPNAME_BG_COLOR] as Int
                val animator = ValueAnimator.ofArgb(startColor, endColor)
                animator.addUpdateListener { it ->
                    cardView.setCardBackgroundColor(it.animatedValue as Int)
                }
                animator
            }
            endValues.view is Chip -> {
                val chip = endValues.view as Chip
                val startColor = startValues.values[PROPNAME_BG_COLOR] as Int
                val endColor = endValues.values[PROPNAME_BG_COLOR] as Int
                val animator = ValueAnimator.ofArgb(startColor, endColor)
                animator.addUpdateListener { it ->
                    chip.chipBackgroundColor = ColorStateList.valueOf(it.animatedValue as Int)
                }
                animator
            }
            else -> null
        }
    }
}