package taila.made.reframe.ui.transitions

import android.animation.Animator
import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.design.card.MaterialCardView
import android.support.transition.Transition
import android.support.transition.TransitionValues
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import taila.made.reframe.R
import taila.made.reframe.ui.elevationAnimator

/**
 * Transition for animating background Elevation
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
class ElevationTransition : Transition {

    companion object {
        private const val TAG = "TT:UI:EelevationTrans"
        private const val PROPNAME_ELEVATION = "taila.made.reframe.ui:ElevationTransition:elevation"
        private val TRANSITION_PROPERTIES = arrayOf(PROPNAME_ELEVATION)


        fun makeExtrasBundle(startElevation: Int): Bundle {
            return Bundle().apply { putInt(PROPNAME_ELEVATION, startElevation) }
        }

        fun makeExtrasBundle(view: View): Bundle {
            return when (view) {
                is MaterialCardView -> {
                    Bundle().apply {
                        putFloat(PROPNAME_ELEVATION, view.cardElevation)
                    }
                }
                else -> {
                    Bundle().apply {
                        putFloat(PROPNAME_ELEVATION, view.elevation)
                    }
                }
            }
        }
    }

    constructor() {}

    @TargetApi(Build.VERSION_CODES.KITKAT)
    constructor(startValuesExtras: Bundle? = null) {
        this.startValuesExtras = startValuesExtras
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    constructor(startElevation: Float, endElevation: Float) {
        this.startElevation = startElevation
        this.endElevation = endElevation
    }

    var startValuesExtras: Bundle? = null


    var startElevation: Float = 0f

    var endElevation: Float = 8f

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.Elevation)
        startElevation = ta.getDimensionPixelSize(R.styleable.Elevation_startElevation, 0).toFloat()
        endElevation = ta.getDimensionPixelSize(R.styleable.Elevation_endElevation, 8).toFloat()
        ta.recycle()
    }

    override fun captureStartValues(transitionValues: TransitionValues) {
        if (startValuesExtras != null) {
            transitionValues.values[PROPNAME_ELEVATION] = startValuesExtras!![PROPNAME_ELEVATION]
        } else {
            val view = transitionValues.view
            try {
                when (view) {
                    is MaterialCardView -> {
                        transitionValues.values[PROPNAME_ELEVATION] = view.cardElevation
                    }
                    else -> {
                        transitionValues.values[PROPNAME_ELEVATION] = view.elevation
                    }
                }
            } catch (e: Exception) {
                Log.d(TAG, e.localizedMessage)
                transitionValues.values[PROPNAME_ELEVATION] = startElevation
            }
        }
    }

    override fun captureEndValues(transitionValues: TransitionValues) {
        val view = transitionValues.view
        try {
            when (view) {
                is MaterialCardView -> {
                    transitionValues.values[PROPNAME_ELEVATION] = view.cardElevation
                }
                else -> {
                    transitionValues.values[PROPNAME_ELEVATION] = view.elevation
                }
            }
        } catch (e: Exception) {
            Log.d(TAG, e.localizedMessage)
            transitionValues.values[PROPNAME_ELEVATION] = endElevation
        }
    }

    override fun createAnimator(sceneRoot: ViewGroup, startValues: TransitionValues?, endValues: TransitionValues?): Animator? {
        Log.d(TAG, "createAnimator()")
        if (startValues == null || endValues == null) {
            return null
        }

        startElevation = startValues.values[PROPNAME_ELEVATION] as Float
        endElevation = endValues.values[PROPNAME_ELEVATION] as Float

        val view = endValues.view

        return when (view) {
            is MaterialCardView -> {
                view.elevationAnimator(floatArrayOf(startElevation, endElevation))
            }
            else -> {
                view.elevationAnimator(floatArrayOf(startElevation, endElevation))
            }
        }
    }

}