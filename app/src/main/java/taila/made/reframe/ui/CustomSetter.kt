package taila.made.reframe.ui

import android.databinding.BindingAdapter
import android.support.annotation.ColorRes
import android.support.annotation.DimenRes
import android.view.View

@BindingAdapter(value = ["startColor", "endColor", "cornerRadius"], requireAll = false)
fun View.gradientDrawableBg(@ColorRes startColorResId: Int, @ColorRes endColorResId: Int, @DimenRes cornerRadius: Float = 0f) {
    this.background = UIUtil.gradientDrawableRect(cornerRadius, startColor = startColorResId, endColor = endColorResId)
}
