package taila.made.reframe.ui.transitions

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.ColorInt
import android.support.design.button.MaterialButton
import android.support.design.card.MaterialCardView
import android.support.design.chip.Chip
import android.support.transition.Transition
import android.support.transition.TransitionValues
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import taila.made.reframe.R
import taila.made.reframe.ui.bgColorAnimator

/**
 * Transition for animating background color
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
class BackgroundColorChangeTransition : Transition {

    companion object {
        private const val TAG = "TT:UI:BgColorTrans"
        private const val PROPNAME_BG_COLOR = "taila.made.reframe.ui:BackgroundColorChangeTransition:bgColor"
        private val TRANSITION_PROPERTIES = arrayOf(PROPNAME_BG_COLOR)


        fun makeExtrasBundle(@ColorInt startColor: Int): Bundle {
            return Bundle().apply { putInt(PROPNAME_BG_COLOR, startColor) }
        }

        fun makeExtrasBundle(view: View): Bundle {
            return when (view) {
                is Chip -> {
                    Bundle().apply {
                        putInt(PROPNAME_BG_COLOR, view.chipBackgroundColor!!.defaultColor)
                    }
                }
                is MaterialCardView -> {
                    Bundle().apply {
                        putInt(PROPNAME_BG_COLOR, view.cardBackgroundColor.defaultColor)
                    }
                }
                is MaterialButton -> {
                    Bundle().apply {
                        putInt(PROPNAME_BG_COLOR, view.backgroundTintList!!.defaultColor)
                    }
                }
                else -> {
                    Bundle().apply {
                        putInt(PROPNAME_BG_COLOR, (view.background as ColorDrawable).color)
                    }
                }
            }
        }
    }

    constructor() {}

    @TargetApi(Build.VERSION_CODES.KITKAT)
    constructor(startValuesExtras: Bundle? = null) {
        this.startValuesExtras = startValuesExtras
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    constructor(@ColorInt startColor: Int, @ColorInt endColor: Int) {
        this.startColor = startColor
        this.endColor = endColor
    }

    var startValuesExtras: Bundle? = null

    @ColorInt
    var startColor: Int = Color.WHITE
    @ColorInt
    var endColor: Int = Color.BLACK

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.Radius)
        startColor = ta.getColor(R.styleable.bgColor_startBgColor, ContextCompat.getColor(context, R.color.colorPrimary))
        endColor = ta.getColor(R.styleable.bgColor_endBgColor, ContextCompat.getColor(context, R.color.colorAccent))
        ta.recycle()
    }

    override fun captureStartValues(transitionValues: TransitionValues) {
        if (startValuesExtras != null) {
            transitionValues.values[PROPNAME_BG_COLOR] = startValuesExtras!![PROPNAME_BG_COLOR]
        } else {
            val view = transitionValues.view
            try {
                when (view) {
                    is Chip -> {
                        transitionValues.values[PROPNAME_BG_COLOR] = view.chipBackgroundColor!!.defaultColor
                    }
                    is MaterialCardView -> {
                        transitionValues.values[PROPNAME_BG_COLOR] = view.cardBackgroundColor.defaultColor
                    }
                    is MaterialButton -> {
                        transitionValues.values[PROPNAME_BG_COLOR] = view.backgroundTintList!!.defaultColor
                    }
                    else -> {
                        transitionValues.values[PROPNAME_BG_COLOR] = (view.background as ColorDrawable).color
                    }
                }
            } catch (e: Exception) {
                Log.d(TAG, e.localizedMessage)
                transitionValues.values[PROPNAME_BG_COLOR] = startColor
            }
        }
    }

    override fun captureEndValues(transitionValues: TransitionValues) {
        val view = transitionValues.view
        try {
            when (view) {
                is Chip -> {
                    transitionValues.values[PROPNAME_BG_COLOR] = view.chipBackgroundColor!!.defaultColor
                }
                is MaterialCardView -> {
                    transitionValues.values[PROPNAME_BG_COLOR] = view.cardBackgroundColor.defaultColor
                }
                is MaterialButton -> {
                    transitionValues.values[PROPNAME_BG_COLOR] = view.backgroundTintList!!.defaultColor
                }
                else -> {
                    transitionValues.values[PROPNAME_BG_COLOR] = (view.background as ColorDrawable).color
                }
            }
        } catch (e: Exception) {
            Log.d(TAG, e.localizedMessage)
            transitionValues.values[PROPNAME_BG_COLOR] = endColor
        }
    }

    override fun createAnimator(sceneRoot: ViewGroup, startValues: TransitionValues?, endValues: TransitionValues?): Animator? {
        Log.d(TAG, "createAnimator()")
        if (startValues == null || endValues == null) {
            return null
        }

        startColor = startValues.values[PROPNAME_BG_COLOR] as Int
        endColor = endValues.values[PROPNAME_BG_COLOR] as Int

        val view = endValues.view

        return when (view) {
            is Chip -> {
                view.bgColorAnimator(intArrayOf(startColor, endColor))
            }
            is MaterialCardView -> {
                view.bgColorAnimator(intArrayOf(startColor, endColor))
            }
            is MaterialButton -> {
                view.bgColorAnimator(intArrayOf(startColor, endColor))
            }
            else -> {
                ValueAnimator.ofArgb(startColor, endColor).apply {
                    addUpdateListener {
                        view.setBackgroundColor(animatedValue as Int)
                    }
                }
            }
        }
    }

}