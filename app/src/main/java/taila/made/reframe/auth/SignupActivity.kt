package taila.made.reframe.auth

import android.animation.AnimatorSet
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.transition.ArcMotion
import android.support.transition.AutoTransition
import android.support.v7.app.AppCompatActivity
import android.view.animation.DecelerateInterpolator
import android.view.animation.OvershootInterpolator
import dagger.android.AndroidInjection
import taila.made.reframe.R
import taila.made.reframe.databinding.ActivitySignupBinding
import taila.made.reframe.ui.AnimationUtil

class SignupActivity : AppCompatActivity() {

    lateinit var binding: ActivitySignupBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
    }

    fun setupViewsForEnterAnimation() {
        binding.tvLetsGetYouStarted.alpha = 0f
        binding.etNameWrapper.alpha = 0f
        binding.etEmailWrapper.alpha = 0f
        binding.etPasswordWrapper.alpha = 0f
    }

    override fun onStart() {
        super.onStart()
        val ivLogoTransition = AutoTransition()
        ivLogoTransition.setPathMotion(ArcMotion())
        ivLogoTransition.duration = AnimationUtil.DURATION_MOBILE_EXITING
        ivLogoTransition.interpolator = OvershootInterpolator()
        ivLogoTransition.addTarget(binding.ivLogo)

        setupViewsForEnterAnimation()
        onEnterAnimation()
    }

    fun onEnterAnimation() {
        val delayGap = 70L
        val duration = AnimationUtil.DURATION_MOBILE_TYPICAL
        val tvHeyYaEnterAnim = AnimationUtil.creepIn(binding.tvHeyYa, direction = AnimationUtil.DIRECTION_DOWN, translationAmountInDp = 16, duration = duration)
        val tvLetsGetYouStartedAnim = AnimationUtil.creepIn(binding.tvLetsGetYouStarted, direction = AnimationUtil.DIRECTION_DOWN, translationAmountInDp = 16, duration = duration)
        val etNameEnterAnim = AnimationUtil.creepIn(binding.etNameWrapper, direction = AnimationUtil.DIRECTION_DOWN, translationAmountInDp = 16, duration = duration)
        val etEmailEnterAnim = AnimationUtil.creepIn(binding.etEmailWrapper, direction = AnimationUtil.DIRECTION_DOWN, translationAmountInDp = 16, duration = duration)
        val etPasswordEnterAnim = AnimationUtil.creepIn(binding.etPasswordWrapper, direction = AnimationUtil.DIRECTION_DOWN, translationAmountInDp = 16, duration = duration)
        tvLetsGetYouStartedAnim.startDelay = delayGap * 1
        etNameEnterAnim.startDelay = delayGap * 2
        etEmailEnterAnim.startDelay = delayGap * 3
        etPasswordEnterAnim.startDelay = delayGap * 4

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(tvHeyYaEnterAnim, tvLetsGetYouStartedAnim, etNameEnterAnim, etEmailEnterAnim, etPasswordEnterAnim)
        animatorSet.interpolator = DecelerateInterpolator()
        animatorSet.start()
    }
}
