package taila.made.reframe.auth

import android.app.Application
import android.arch.lifecycle.*
import android.content.Context
import android.content.Intent
import com.crashlytics.android.Crashlytics
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.AuthResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import taila.made.reframe.App
import taila.made.reframe.utils.FirebaseUtil
import javax.inject.Inject

class AuthVM(app: Application) : AndroidViewModel(app), LifecycleObserver {

    @Inject
    lateinit var firebaseUtil: FirebaseUtil
    lateinit var googleSignInClient: GoogleSignInClient

    private val rxDisposables = CompositeDisposable()

    val createUserResponse = MutableLiveData<AuthResult>()
    val createUserError = MutableLiveData<Throwable>()
    val loginResponse = MutableLiveData<AuthResult>()
    val loginError = MutableLiveData<Throwable>()
    val googleSignInIntent = MutableLiveData<Intent>()


    init {
        App.component.inject(this)
    }

    override fun onCleared() {
        rxDisposables.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun setupGoogleAuth() {
        // Configure sign-in to request the user's ID, email address, and basic profile.
        // ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        // Build a GoogleSignInClient with the options specified by gso.
        googleSignInClient = GoogleSignIn.getClient(getApplication() as Context, gso)
    }

    fun signInWithGoogle() {
        googleSignInIntent.value = googleSignInClient.signInIntent
    }

    fun signupWithEmailAndPassword(email: String, password: String) {
        rxDisposables.add(
            firebaseUtil.rxCreateUserWithEmailAndPassword(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ authResult ->
                    createUserResponse.value = authResult
                }, { throwable ->
                    Crashlytics.logException(throwable)
                    throwable.printStackTrace()
                    createUserError.value = throwable
                })
        )
    }

    fun login(email: String, password: String) {
        rxDisposables.add(
            firebaseUtil.rxSignInWithEmailAndPassword(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ authResult ->
                    loginResponse.value = authResult
                }, { throwable ->
                    Crashlytics.logException(throwable)
                    throwable.printStackTrace()
                    loginError.value = throwable
                })
        )
    }
}