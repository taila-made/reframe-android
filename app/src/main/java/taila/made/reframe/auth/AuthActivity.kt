package taila.made.reframe.auth

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.button.MaterialButton
import android.support.transition.*
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.EditText
import android.widget.ImageButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import dagger.android.AndroidInjection
import taila.made.reframe.R
import taila.made.reframe.databinding.ActivityAuthSceneSignupBinding
import taila.made.reframe.home.MainActivity
import taila.made.reframe.startActivityAndClearStack
import taila.made.reframe.ui.AnimationUtil
import taila.made.reframe.ui.transitions.*

class AuthActivity : AppCompatActivity() {

    companion object {
        const val STATE_SIGNUP_COLLAPSED = 0
        const val STATE_SIGNUP_EXPANDED = 1
        const val STATE_LOGIN_COLLAPSED = 2
        const val STATE_LOGIN_EXPANDED = 3
        const val TAG = "TT:AuthAct"

        const val RC_GOOGLE_SIGN_IN = 31330
    }

    lateinit var vm: AuthVM
    lateinit var binding: ActivityAuthSceneSignupBinding
    var state = STATE_SIGNUP_COLLAPSED

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth_scene_signup)
        vm = ViewModelProviders.of(this).get(AuthVM::class.java)
        lifecycle.addObserver(vm)
        setupTransitions()
        setupListeners()
        observe()
    }

    private fun gotoHomeActivity() {
        startActivityAndClearStack(Intent(this, MainActivity::class.java))
    }

    private fun observe() {
        vm.createUserResponse.observe(this, Observer { authResult ->
            if (authResult != null) {
                // TODO: Start splash screen to load user and all that
                startActivity(Intent(this, MainActivity::class.java))
            }
        })

        vm.createUserError.observe(this, Observer { throwable ->
            // TODO: Show user friendly error message/state
            Log.d(TAG, throwable!!.localizedMessage)
        })

        vm.loginResponse.observe(this, Observer { authResult ->
            if (authResult != null) {
                // TODO: Start splash screen to load user and all that
                gotoHomeActivity()
            }
        })

        vm.loginError.observe(this, Observer { throwable ->
            Log.d(TAG, throwable!!.localizedMessage)
        })

        vm.googleSignInIntent.observe(this, Observer { signInIntent ->
            startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN)
        })
    }

    // TODO: Figure out why I have to call this upon each scene change - Also without having to use findViewById -> Rebind Databinding?
    fun setupListeners() {
        sceneSignupCollapsed = Scene.getSceneForLayout(binding.root as ViewGroup, R.layout.activity_auth_scene_signup, this)
        sceneSignupExpanded = Scene.getSceneForLayout(binding.root as ViewGroup, R.layout.activity_auth_scene_signup_expanded, this)
        sceneLoginCollapsed = Scene.getSceneForLayout(binding.root as ViewGroup, R.layout.activity_auth_scene_login, this)
        sceneLoginExpanded = Scene.getSceneForLayout(binding.root as ViewGroup, R.layout.activity_auth_scene_login_expanded, this)
        findViewById<View>(R.id.btnSignup).setOnClickListener {
            if (state == STATE_SIGNUP_COLLAPSED) {
                TransitionManager.go(sceneSignupExpanded, signupCollapsedToExpandedTransition)
            } else if (state == STATE_LOGIN_COLLAPSED) {
                TransitionManager.go(sceneLoginExpanded, loginCollapsedToExpandedTransition)
            }
        }
        findViewById<View>(R.id.btnLoginInstead).setOnClickListener {
            when (state) {
                STATE_SIGNUP_COLLAPSED -> TransitionManager.go(sceneLoginCollapsed, signupCollapsedToLoginCollapsedTransition)
                STATE_LOGIN_COLLAPSED -> TransitionManager.go(sceneSignupCollapsed, loginCollapsedToSignupCollapsedTransition)
                STATE_LOGIN_EXPANDED -> TransitionManager.go(sceneSignupExpanded, loginExpandedToSignupExpandedTransition)
                STATE_SIGNUP_EXPANDED -> TransitionManager.go(sceneLoginExpanded, signupExpandedToLoginExpandedTransition)
            }
        }
        findViewById<MaterialButton>(R.id.btnSignupDone)?.setOnClickListener {
            when (state) {
                STATE_SIGNUP_EXPANDED -> {
                    // TODO: Sanitize inputs
                    vm.signupWithEmailAndPassword(email = emailText(), password = passwordText())
                }
            }
        }
        findViewById<ImageButton>(R.id.btnGoogleSignup)?.setOnClickListener {
            when (state) {
                STATE_SIGNUP_EXPANDED -> {
                    vm.signInWithGoogle()
                }
            }
        }
    }

    override fun onBackPressed() {
        when (state) {
            STATE_SIGNUP_EXPANDED -> TransitionManager.go(sceneSignupCollapsed, signupExpandedToCollapsedTransition)
            STATE_LOGIN_EXPANDED -> TransitionManager.go(sceneLoginCollapsed, loginExpandedToCollapsedTransition)
            else -> super.onBackPressed()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                // Signed in successfully, show authenticated UI.
                gotoHomeActivity()
            } catch (e: ApiException) {
                // The ApiException status code indicates the detailed failure reason.
                Log.w(TAG, "signInResult:failed code=${e.statusCode} and localized message = ${e.localizedMessage}")
            }
        }
    }

    fun btnSignupDone() = findViewById<MaterialButton>(R.id.btnSignupDone)
    fun emailText() = findViewById<EditText>(R.id.etEmail).text.toString()
    fun passwordText() = findViewById<EditText>(R.id.etPassword).text.toString()
    fun passwordConfirmationText() = findViewById<EditText>(R.id.etPasswordConfirm).text.toString()

    /** ANIMATIONS */
    private lateinit var signupCollapsedToExpandedTransition: TransitionSet
    private lateinit var signupExpandedToCollapsedTransition: TransitionSet
    private lateinit var signupCollapsedToLoginCollapsedTransition: TransitionSet
    private lateinit var loginCollapsedToSignupCollapsedTransition: TransitionSet
    private lateinit var signupExpandedToLoginExpandedTransition: TransitionSet
    private lateinit var loginExpandedToSignupExpandedTransition: TransitionSet
    private lateinit var loginCollapsedToExpandedTransition: TransitionSet
    private lateinit var loginExpandedToCollapsedTransition: TransitionSet
    private lateinit var signupMorph: TransitionSet
    private lateinit var logoAndTitleChangeBounds: Transition
    private lateinit var sceneSignupCollapsed: Scene
    private lateinit var sceneSignupExpanded: Scene
    private lateinit var sceneLoginExpanded: Scene
    private lateinit var sceneLoginCollapsed: Scene

    private fun setupTransitions() {
        signupMorph = TransitionSet().buildMorphTransition(targetIds = intArrayOf(R.id.btnSignup))
        logoAndTitleChangeBounds = ChangeBounds().apply {
            addTargets(targetIds = intArrayOf(R.id.ivLogo, R.id.tvAppName))
        }
        setupSignupCollapsedToExpandedTransition()
        setupSignupExpandedToCollapsedTransition()
        setupSignupExpandedToLoginExpandedTransition()
        setupLoginExpandedToSignupExpandedTransition()
        setupLoginExpandedToCollapsedTransition()
        setupLoginCollapsedToExpandedTransition()
        setupSignupCollapsedToLoginCollapsedTransition()
        setupLoginCollapsedToSignupCollapsedTransition()
    }

    private fun setupSignupCollapsedToExpandedTransition() {
        val phase1 = TransitionSet().configure(transitionsToAdd = arrayOf(signupMorph, logoAndTitleChangeBounds))
        val fadeTextFields = Fade().apply {
            propagation = SidePropagation().apply {
                setSide(Gravity.BOTTOM)
                setPropagationSpeed(0.8f)
            }
            addTargets(targetIds = intArrayOf(R.id.etEmailWrapper, R.id.etPasswordWrapper, R.id.etPasswordConfirmWrapper))
        }
        val phase2 = TransitionSet().configure(transitionsToAdd = arrayOf(fadeTextFields))
        val fadeButtons = Fade().apply {
            addTargets(intArrayOf(R.id.btnGoogleSignup, R.id.btnFbSignup, R.id.btnSignupDone))
            propagation = SidePropagation().apply {
                setSide(Gravity.END)
            }
        }
        val phase3 = TransitionSet().configure(transitionsToAdd = arrayOf(fadeButtons))
        signupCollapsedToExpandedTransition = TransitionSet().configure(transitionsToAdd = arrayOf(phase1, phase2, phase3)).apply {
            ordering = TransitionSet.ORDERING_SEQUENTIAL
            interpolator = OvershootInterpolator(0.9f)
            addListener(object : TransitionListenerAdapter() {
                override fun onTransitionStart(transition: Transition) {
                    state = STATE_SIGNUP_EXPANDED
                    setupListeners()
                }
            })
        }
    }

    private fun setupSignupExpandedToCollapsedTransition() {
        val phase1 = TransitionSet().apply {
            addTransition(TransitionSet().configure(transitionsToAdd = arrayOf(signupMorph, logoAndTitleChangeBounds)))
            addTransition(TextColorTransition(startTextColor = ContextCompat.getColor(this@AuthActivity, android.R.color.transparent),
                    endTextColor = ContextCompat.getColor(this@AuthActivity, android.R.color.white)).apply {
                addTarget(R.id.btnSignup)
                duration = 600
            })
        }

        val fade = Fade().apply {
            propagation = SidePropagation().apply {
                setSide(Gravity.BOTTOM)
            }
            duration = AnimationUtil.DURATION_MOBILE_EXITING
            addTargets(targetIds = intArrayOf(R.id.etEmailWrapper, R.id.etPasswordWrapper, R.id.etPasswordConfirmWrapper))
        }

        val phase2 = TransitionSet().configure(transitionsToAdd = arrayOf(fade))
        signupExpandedToCollapsedTransition = TransitionSet().configure(transitionsToAdd = arrayOf(phase1, phase2)).apply {
            interpolator = AccelerateDecelerateInterpolator()
            addListener(object : TransitionListenerAdapter() {
                override fun onTransitionStart(transition: Transition) {
                    state = STATE_SIGNUP_COLLAPSED
                    setupListeners()
                }
            })
        }
    }

    private fun setupSignupExpandedToLoginExpandedTransition() {
        signupExpandedToLoginExpandedTransition = TransitionSet().apply {
            addTransition(ChangeBounds())
            addTransition(BackgroundColorChangeTransition().apply {
                addTarget(R.id.btnSignup)
            })
            addListener(object : TransitionListenerAdapter() {
                override fun onTransitionStart(transition: Transition) {
                    state = STATE_LOGIN_EXPANDED
                    setupListeners()
                }
            })
        }
    }

    private fun setupLoginExpandedToSignupExpandedTransition() {
        loginExpandedToSignupExpandedTransition = TransitionSet().apply {
            addTransition(ChangeBounds())
            addTransition(BackgroundColorChangeTransition().apply {
                addTarget(R.id.btnSignup)
            })
            addListener(object : TransitionListenerAdapter() {
                override fun onTransitionStart(transition: Transition) {
                    state = STATE_SIGNUP_EXPANDED
                    setupListeners()
                }
            })
        }
    }

    private fun setupLoginExpandedToCollapsedTransition() {
        val phase1 = TransitionSet().apply {
            addTransition(TransitionSet().configure(transitionsToAdd = arrayOf(signupMorph, logoAndTitleChangeBounds)))
            addTransition(TextColorTransition(startTextColor = ContextCompat.getColor(this@AuthActivity, android.R.color.transparent),
                    endTextColor = ContextCompat.getColor(this@AuthActivity, android.R.color.white)).apply {
                addTarget(R.id.btnSignup)
                duration = 600
            })
        }

        val fade = Fade().apply {
            propagation = SidePropagation().apply {
                setSide(Gravity.BOTTOM)
            }
            duration = AnimationUtil.DURATION_MOBILE_EXITING
            addTargets(targetIds = intArrayOf(R.id.etEmailWrapper, R.id.etPasswordWrapper))
        }

        val phase2 = TransitionSet().configure(transitionsToAdd = arrayOf(fade))
        loginExpandedToCollapsedTransition = TransitionSet().configure(transitionsToAdd = arrayOf(phase1, phase2)).apply {
            interpolator = AccelerateDecelerateInterpolator()
            addListener(object : TransitionListenerAdapter() {
                override fun onTransitionStart(transition: Transition) {
                    state = STATE_LOGIN_COLLAPSED
                    setupListeners()
                }
            })
        }
    }

    private fun setupLoginCollapsedToExpandedTransition() {
        val phase1 = TransitionSet().configure(transitionsToAdd = arrayOf(signupMorph, logoAndTitleChangeBounds))
        val fadeTextFields = Fade().apply {
            propagation = SidePropagation().apply {
                setSide(Gravity.BOTTOM)
                setPropagationSpeed(0.8f)
            }
            addTargets(targetIds = intArrayOf(R.id.etEmailWrapper, R.id.etPasswordWrapper))
        }
        val phase2 = TransitionSet().configure(transitionsToAdd = arrayOf(fadeTextFields))
        val fadeButtons = Fade().apply {
            addTargets(intArrayOf(R.id.btnGoogleSignup, R.id.btnFbSignup, R.id.btnSignupDone))
            propagation = SidePropagation().apply {
                setSide(Gravity.END)
            }
        }
        val phase3 = TransitionSet().configure(transitionsToAdd = arrayOf(fadeButtons))
        loginCollapsedToExpandedTransition = TransitionSet().configure(transitionsToAdd = arrayOf(phase1, phase2, phase3)).apply {
            ordering = TransitionSet.ORDERING_SEQUENTIAL
            interpolator = OvershootInterpolator(0.9f)
            addListener(object : TransitionListenerAdapter() {
                override fun onTransitionStart(transition: Transition) {
                    state = STATE_LOGIN_EXPANDED
                    setupListeners()
                }
            })
        }
    }

    private fun setupSignupCollapsedToLoginCollapsedTransition() {
        signupCollapsedToLoginCollapsedTransition = TransitionSet().configure(transitionsToAdd = arrayOf(BackgroundColorChangeTransition().apply {
            addTarget(R.id.btnSignup)
            addListener(object : TransitionListenerAdapter() {
                override fun onTransitionStart(transition: Transition) {
                    state = STATE_LOGIN_COLLAPSED
                    setupListeners()
                }
            })
        }))
    }

    private fun setupLoginCollapsedToSignupCollapsedTransition() {
        loginCollapsedToSignupCollapsedTransition = TransitionSet().configure(transitionsToAdd = arrayOf(BackgroundColorChangeTransition().apply {
            addTarget(R.id.btnSignup)
            addListener(object : TransitionListenerAdapter() {
                override fun onTransitionStart(transition: Transition) {
                    state = STATE_SIGNUP_COLLAPSED
                    setupListeners()
                }
            })
        }))
    }

}
