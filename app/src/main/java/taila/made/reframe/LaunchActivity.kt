package taila.made.reframe

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection
import taila.made.reframe.auth.AuthActivity
import taila.made.reframe.home.MainActivity
import taila.made.reframe.utils.FirebaseUtil
import javax.inject.Inject

class LaunchActivity : AppCompatActivity() {

    @Inject
    lateinit var firebaseUtil: FirebaseUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        if(firebaseUtil.isLoggedIn()) {
            startActivityAndClearStack(Intent(this, MainActivity::class.java))
        } else {
            startActivityAndClearStack(Intent(this, AuthActivity::class.java))
        }
    }

}